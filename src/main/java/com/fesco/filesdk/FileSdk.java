package com.fesco.filesdk;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;

import java.io.File;
import java.io.IOException;

/**
 * <descption>SDK</descption>
 * @date ：2017/4/24 16:46
 * @author：梅海波<meihaibo13186@sinosoft.com.cn>
 * @version:0.1
 */
public class FileSdk {
    /**
     * <descption>下载，出入两个参数，其他参数再url拼？的形式</descption>
     * @date ：2017/4/24 16:45
     * @author：梅海波<meihaibo13186@sinosoft.com.cn>
     * @version:0.1
     */
    public static String uploadFile(final String targetURL, final File targetFile) {
        PostMethod filePost = new PostMethod(targetURL);
        String fileId = "";
        try   {
            Part[] parts = { new FilePart(targetFile.getName(), targetFile)};
            filePost.setRequestEntity(new MultipartRequestEntity(parts,filePost.getParams()));
            HttpClient client = new HttpClient();
            client.getHttpConnectionManager().getParams().setConnectionTimeout(5000);
            int status = client.executeMethod(filePost);
            if (status == HttpStatus.SC_OK)     {
                System.out.println("上传成功");      // 上传成功
            }else{
                System.out.println("上传失败");      // 上传失败
            }
            String result = filePost.getResponseBodyAsString();
            Object re = JSONObject.parseObject(result).get("data");
            if (re != null){
                fileId = re.toString();
            }
        }catch (Exception ex) {
            ex.printStackTrace();
        }    finally   {
            filePost.releaseConnection();
        }
        return fileId;
    }

    /**
     * <descption></descption>
     * @date ：2017/4/24 17:03
     * @author：梅海波<meihaibo13186@sinosoft.com.cn>
     * @param targerURI 其他参数用?拼接
     * @return
     * @throws Exception
     */
    public static final byte[] downFile(final String targerURI)throws Exception{

        //String file = getPicUrlByImageId(fileId);
        GetMethod get = new GetMethod(targerURI);

        byte[] bytes;
        try {
            HttpClient client = new HttpClient();
            client.getHttpConnectionManager().getParams().setConnectionTimeout(5000);
            int status = client.executeMethod(get);
            if(status != 200) {
                throw new Exception(get.getResponseBodyAsString());
            }
            bytes = get.getResponseBody();
        } catch (IOException e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        } finally {
            get.releaseConnection();
        }

        return bytes;
    }


    public static void main(String[] args) throws Exception{
        FileSdk fileSdk = new FileSdk();
        String fileId = fileSdk.uploadFile("https://openapi.fesco.com.cn/fescoOpenFile/upload.do",new File("d:/workspace/3RD-PARTY-LICENSE.txt"));
        System.out.print(fileId);
        byte[] bytes = fileSdk.downFile("https://openapi.fesco.com.cn/fescoOpenFile/view.do?fileid="+fileId);
        System.out.print(bytes.length);
    }
}
